NAME = icsoft/squid
IMAGEVERSION = v4.4_19.04_20190525
BUILDTIME = `date +%FT%T%z`
LISTENPORT = 3128
LOCALVOL = ${HOME}/docker

.PHONY: all build release clean run tag

all: build

build:
	@echo " --> Building docker container."
	docker build -t ${NAME}:${IMAGEVERSION} --build-arg LISTENPORT=${LISTENPORT} --rm ./

release:
	@echo " --> Re-tagging  docker image to latest, for upload to docker hub."
	docker tag ${NAME}:${IMAGEVERSION} ${NAME}:latest
	docker push ${NAME}

clean:
	@echo " --> Cleaning up dangling temporary files."
	docker system prune
	-rm -rf ${LOCALVOL}/squid
	-docker rm -f squidtest
	-docker rmi ${NAME}:${IMAGEVERSION}
	-docker rmi ${NAME}:latest

run:
	@echo "--> Running Squid container for testing."
	@echo "Note: Directories created for persistance in Roots home."
	mkdir -p ${LOCALVOL}/squid/cache
	mkdir -p ${LOCALVOL}/squid/conf.d
	mkdir -p ${LOCALVOL}/squid/log
	docker run --name squidtest -d \
	--publish ${LISTENPORT}:${LISTENPORT} \
	--volume ${LOCALVOL}/squid/conf.d:/etc/squid/conf.d \
	--volume ${LOCALVOL}/squid/cache:/var/spool/squid \
	--volume ${LOCALVOL}/squid/log:/var/log/squid \
	icsoft/squid:${IMAGEVERSION}

tag:
	@echo " --> Tagging via git with version ${IMAGEVERSION}"
	git tag -a ${IMAGEVERSION} -m "release ${IMAGEVERSION}"
