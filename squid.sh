#!/bin/sh

CFGDIR="/etc/squid/conf.d"
CFGFILE="docker.conf"
CACHEDIR="/var/spool/squid"
LOGDIR="/var/log/squid/"

#Load squid application configuration files if the directory is empty.
if [ "$(ls -A $CFGDIR)" ]; then
    echo "--> Pre-existing $CFGFILE config file found in $CFGDIR"
else
    echo "--> Creating $CFGFILE configuration file in $CFGDIR"
    cat >> $CFGDIR/$CFGFILE << EOF
#
# Squid configuration settings for access through docker.
# Autocreated on empty directory.
#

# Change this to subnet of your docker container if it differs from default.
# This subnet is already part of the default config.
# acl localnet src 172.16.0.0/12
http_access allow localnet

# Squid caching directory
cache_dir ufs $CACHEDIR 100 16 256
EOF
    
    echo "--> Changing ownership of config file to user/group proxy"
    chown -R proxy $CFGDIR $CACHEDIR $LOGDIR
    chmod 660 -Rf $CFGDIR/$CFGFILE
fi

if [ -d $CACHEDIR/00 ]; then
    echo "--> pre-existing cache found"
else
    echo "--> Initializing cache"
    exec /usr/sbin/squid -N -z
fi

exec /usr/sbin/squid -NYCd 1 >> $LOGDIR/run.log 2>&1
