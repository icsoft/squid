# Squid

## Summary
Squid is a very configurable, advanced proxy server.

This container is based on phusion/baseimage and is configured to allow access from the default docker subnet.

This container automagically creates a config file in the mapped /etc/squid/conf.d directory (if it is empty) for you to add your own settings for your LAN.

## Docker Image
### Container Location
The container can be downloaded from Docker Hub: https://hub.docker.com/r/icsoft/squid

### Running the Container
Running the container can be achieved by initially crating the directories for persistent storage for Squid,
(change "~/docker" to your persistent storage directory otherwise it will be accessed in your roots home directory):

	mkdir -p ~/docker/squid/cache
	mkdir -p ~/docker/squid/logs
	mkdir -p ~/docker/squid/conf.d

Downloading and running Squid:

	docker run --name squid -d --restart=always \
		--publish 3128:3128 \
		--volume ~/docker/squid/conf.d:/etc/squid/conf.d \
		--volume ~/docker/squid/cache:/var/spool/squid \
		--volume ~/docker/squid/logs:/var/spool/log \
		icsoft/squid

### Volume Mappings

* "/etc/squid/conf.d" (optional) allows access to the configuration file to add settings to squid.
* "/var/spool/squid" (optional) allows for external cache persistance.
* "/var/spool/log" (optional) Allows access to logs files for debugging etc.

### Squid Configuration
Squid is configured with the default config file for the Ubuntu it is built from, 
You need to allow Squid to allow access from your LAN/PC to the docker subnet, 
so the following has been added to /etc/squid/conf.d/docker.conf:

	#Default config, Allows access from the Docker subnet,
	#change to your docker subnet if you are not running on the fault subnet
	#and to /32 for only access from the one IP, usually the Docker gateway.
	acl localnet src 172.16.0.0/12
	
	#As "localnet" is the acl name above to allow access to HTTP.
	http_access allow localnet
	
	#This is the setup for the external cache directory, comment it out if you don't want a disk cache
	cache_dir ufs /var/spool/squid 100 16 256

### Using the Container
If you are on the same computer as the proxy, set your proxy in your browser to:
    
    127.0.0.1:3128
    
If you are on the LAN that the proxy container is on but not on the same computer, 
set your proxy in your browser to the IP address of the Docker computer, port 3128. 

### Accessing the Container
The container can be accessed in the normal docker way:

	docker exec -it squid bash
	
### Source
The home of this container is at https://bitbucket.org/icsoft/squid
